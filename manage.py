#!/usr/bin/env python
import os

from flask.ext.script import Manager, Server
from gxd import create_app

env = os.environ.get('gxd_ENV', 'dev')
app = create_app(env)

manager = Manager(app)
manager.add_command("server", Server(host="0.0.0.0", port=5000))

if __name__ == "__main__":
    manager.run()
