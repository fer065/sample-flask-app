class Config(object):
    pass

class ProdConfig(Config):
    DEBUG = False

    MONGODB_DB = 'mydatabase'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017

    SECRET_KEY = 'ProductionSecretKey'


class DevConfig(Config):
    DEBUG = True

    MONGODB_DB = 'mydatabase'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017

    SECRET_KEY = 'DevelopmentSecretKey'