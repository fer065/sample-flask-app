from flask import Flask, render_template
from flask.ext.mongoengine import MongoEngine
from flask.ext.security import Security, MongoEngineUserDatastore, \
    UserMixin, RoleMixin, login_required

db = MongoEngine()
# Create app
app = Flask(__name__)

def create_app(env):
    config_name = 'gxd.settings.%sConfig' % env.capitalize()
    app.config.from_object(config_name)
    db.init_app(app)
    # Setup Flask-Security
    user_datastore = MongoEngineUserDatastore(db, User, Role)
    security = Security(app, user_datastore)
    # Create a user to test with
    @app.before_first_request
    def create_user():
        if user_datastore.find_user(email='sample@mail.com') is None:
            user_datastore.create_user(email='sample@mail.com', password='aaaaaa')

    return app


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)

class User(db.Document, UserMixin):
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])



# Views
@app.route('/')
@login_required
def home():
    return render_template('index.html', secret_key=app.config['SECRET_KEY'])
